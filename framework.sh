#!/usr/bin/env bash

# Copyright 2023 Bas v.d. Wiel
#
# Redistribution and use in source and binary forms, with 
# or without modification, are permitted provided that 
# the following conditions are met:
#
# 1. Redistributions of source code must retain the above 
#    copyright notice, this list of conditions and the following 
#    disclaimer.
#
# 2. Redistributions in binary form must reproduce the above 
#    copyright notice, this list of conditions and the following 
#    disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its 
#    contributors may be used to endorse or promote products 
#    derived from this software without specific prior written 
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
# CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, 
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
# OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Prevent the running of the script directly even if it is executable
if [[ "$DOSK8SGUARD" -ne "1" ]]; then
  echo "DOSContainer Framework. Do not run this script directly!"
  echo "Use build.sh instead or consult the documentation."
  exit 1
fi

# Function library starts below this line
set -eu

# Wrapper for echo that takes care of the LOGLEVEL variable
function message() {
    local message="$1"
    local verbosity="$2"
    : "${LOGLEVEL:=0}"
    # Check if log level and verbosity are within the expected range
    if [[ "$LOGLEVEL" =~ ^[0-2]$ && "$verbosity" =~ ^[0-2]$ ]]; then
        if [[ "$verbosity" -le "$LOGLEVEL" ]]; then
            case "$verbosity" in
              "0")
                echo "${message}"
              ;;
              "1")
                echo "${message}"
              ;;
              "2")
                echo "${message}"
              ;;
            esac
        fi
    else
        echo "Error: Log level and verbosity must be integers between 0 and 2."
        return 1
    fi
}

# Helper function to check if a path is inside another
function path_is_inside() {
  # Check if two parameters are provided
  if [[ "$#" -ne 2 ]]; then
    message "Error: wrong call to path_is_inside(), not enough params." "2"
    return 1
  fi

  # Check if paths exist
  if [[ ! -e "$1" ]] || [[ ! -e "$2" ]]; then
    message "Error: wrong call to path_is_inside(), one of the paths does not exist." "2"
    return 1
  fi

  basepath=$("$readlink" -f "$1")
  subpath=$("$readlink" -f "$2")

  # Check if path1 is inside path2
  if [[ "$subpath" == "$basepath"* ]]; then
    message "Path $subpath is inside $basepath." "2"
    return 0
  else
    message "Path $subpath is not inside $basepath." "2"
    return 1
  fi
}

# Cleanup a given directory within $DISTPATH
function clean_path() {
  cleanpath=$("$readlink" -f "$1")
  realdistpath=$("$readlink" -f "$DISTPATH")
  if [[ ! -d "$cleanpath" ]]; then
    message "$cleanpath is not a directory, unable to clean up." "2"
    return 1
  fi
  if path_is_inside "$realdistpath" "$cleanpath"; then
    message "Cleaning up $cleanpath." "2"
    rm -rf "$cleanpath"
  else
    # Refuse to do anything outside of $DISTPATH for security
    message "$cleanpath is not inside $realdistpath. Not cleaning." "2"
    return 1
  fi
}

# Use YAMLLint to validate the input file
function validate_yaml() {
  if [[ ! -r "$1" ]]; then
    message "Error: unable to parse input YAML." "0"
    return 1
  fi

  #"$yamllint" "$1" >/dev/null 2>&1;
  if ! "$yamllint" "$1" > /dev/null ; then
    message "Error: invalid YAML file given, refusing to process." "0"
    return 1
  else
    return 0
  fi
}

# Parse YAML into variables
function parse_yaml() {
  # Check if we got our required parameter
  if [ "$#" -eq 0 ]; then
    return 1
  fi

  # Check if the parameter we got is a readable file
  if [[ ! -r "$1" ]]; then
    return 1
  fi

  # Validate integrity of the file passed
  if ! validate_yaml $1; then
    return 1
  fi

  declare -a -g DRIVERS GAMEFILES
  declare -A -g DISTFILES

  if "$yq" -e 'has("diskname")' "$1" > /dev/null; then
    DISKNAME=$("$yq" -e '.diskname' "$1" | "$tr" -d '"')
  else
    DISKNAME="DOSK8S"
  fi
  DISKLABEL=$("$yq" -e '.disklabel' "$1" | "$tr" -d '"')
  if "$yq" -e 'has("disksize")' "$1" > /dev/null; then
    DISKSIZE=$("$yq" -e '.disksize' "$1")
  else
    DISKSIZE=5
  fi
  if "$yq" -e 'has("os")' "$1" > /dev/null; then
    OS=$("$yq" -e '.os' "$1" | "$tr" -d '"')
  else
    OS="MSDOS622"
  fi
  OSVARIANT=$("$yq" -e '.osvariant' "$1" | "$tr" -d '"')
  AUTOEXEC=$("$yq" -e '.AUTOEXEC' "$1" | "$tr" -d '"')
  CONFIG=$("$yq" -e '.CONFIG' "$1" | "$tr" -d '"')

  if "$yq" -e -r '.cutemouseversion' "$1" > /dev/null 2>&1; then
    CTMOUSE_VERSION=$("$yq" -e '.cutemouseversion' "$1" | "$tr" -d '"')
  fi

  if "$yq" -e -r '.emm386params' "$1" &> /dev/null; then
    emm386params_temp=$("$yq" -e '.emm386params' "$1" | "$tr" -d '"')
    EMM386PARAMS="${emm386params_temp//$/\\$}"
  fi

  if $("$yq" -e -r 'has("drivers")' "$1" > /dev/null 2>&1); then
    readarray -t DRIVERS < <("$yq" -e -r '.drivers[]' "$1")
  fi

  if "$yq" -e -r 'has("gamefiles")' "$1" > /dev/null 2>&1; then
    readarray -t GAMEFILES < <("$yq" -e -r '.gamefiles[]' "$1")
  fi

  if "$yq" -e '.distfiles' "$1" > /dev/null 2>&1; then
    case $YQVERSION in
    [0-3]*)
      message "Detected yq version < 4.0.0" "2"
      distfiles_json=$("$yq" -e '.distfiles' "$1")
      ;;
    [4-9]*)
      message "Detected yq version > 4.0.0" "2"
      distfiles_json=$("$yq" -e '.distfiles' -o json "$1")
      ;;
    *)
      message "Error: something went very wrong with the yq version. Aborting." "0"
      exit 1
      ;;
    esac
    while IFS= read -r line; do
      name=$(echo "$line" | "$jq" -r '.name')
      checksum=$(echo "$line" | "$jq" -r '.checksum')
      # Check if required fields are present in distfiles
      if [[ -z "$name" ]] || [[ -z "$checksum" ]]; then
        message "Error: Missing required fields in distfile entry." "0"
        return 1
      fi
      DISTFILES["$name"]="$checksum"
    done <<< "$(echo "$distfiles_json" | "$jq" -c '.[]')"
  fi
}

function print_intro() {
  case "$OS" in
    "MSDOS400")
      displayos="MS-DOS 4.00"
      ;;
    "MSDOS401")
      displayos="MS-DOS 4.01"
      ;;
    "MSDOS500")
      displayos="MS-DOS 5.00"
      ;;
    "MSDOS600")
      displayos="MS-DOS 6.00"
      ;;
    "MSDOS620")
      displayos="MS-DOS 6.20"
      ;;
    "MSDOS621")
      displayos="MS-DOS 6.21"
      ;;
    "MSDOS622")
      displayos="MS-DOS 6.22"
      ;;
    *)
      displayos="Unknown OS! ($OS)"
      ;;
  esac
  echo "DOSContainer v0.0.3 disk image generator."
  echo "(C)2023 - Bas v.d. Wiel <bas@area536.com>"
  echo "-----------------------------------------"
  echo " Build summary "
  echo "-----------------------------------------"
  echo " Build path       : $BUILDPATH"
  echo " Operating system : $displayos "
  echo " Hard drive image : $DISKNAME "
  echo " Hard drive size  : $DISKSIZE MB"
  echo "-----------------------------------------"
}

# Verify the presence and usability of a CLI tool
function check_utility() {
  local util_name="$1"

  if command -v "$util_name" >/dev/null 2>&1; then
    # Assign path to util variable
    local util_path=$(command -v "$util_name")
    # Use declare to create a global variable with the same name as util_name
    declare -g "$util_name=$util_path"
    message "$util_name found at: $util_path" "2"
    return 0
  else
    message "$util_name not found, cannot continue." "0"
    return 1
  fi
}

# Detection function for yq version
function detect_yq() {
    if check_utility "yq"; then
      # Use grep to extract the version number pattern
      local version_number=$("$yq" --version 2>&1 | "$grep" -oP '\b(v?)([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2})\b' | "$sed" -E 's/^v//')

      if [ -n "$version_number" ]; then
        declare -g YQVERSION="$version_number"
        return 0
      else
         message "Unable to determine version of yq. Assuming 4.x.x. You may see errors!" "0"
         declare -g YQVERSION="4.0.0" # Assuming yq 4.xx since that's the most recent major
         return 0
    fi
  else
    message "Error: The yq utility was not found." "0"
    exit 1
  fi
}

# Detection logic for GNU grep (simple calls to ambiguous grep shouldn't matter)
function detect_gnu_grep() {
  # Check if GNU grep is available as the default system grep
  if command -v grep &> /dev/null && grep --version | grep -q "GNU grep"; then
    grep_path=$(command -v grep)
    message "GNU grep detected at: $grep_path" "2"
    grep="$grep_path"
  elif [ -x /usr/local/bin/grep ]; then
    # Check if /usr/local/bin/grep is the GNU version
    if /usr/local/bin/grep --version | grep -q "GNU grep"; then
      grep_path="/usr/local/bin/grep"
      message "GNU grep detected at: $grep_path" "2"
      grep="$grep_path"
    else
      message "/usr/local/bin/grep is not the GNU version. Make sure you have it installed." "0"
      return 1
    fi
  else
    message "GNU grep not found. Make sure you have it installed." "0"
    return 1
  fi
}


# Specific function to detect GNU sed
function detect_gnu_sed() {
  if command -v sed > /dev/null 2>&1; then
    # Figure out the sed variant
    if sed --version 2>&1 | "$grep" -q 'GNU sed'; then
      sed=$(command -v sed)
      return 0
    else
      # Figure out GNU sed on BSD systems
      if command -v gsed > /dev/null 2>&1; then
        sed=$(command -v gsed)
        return 0
      else
        # Fatal error, won't continue without GNU set
        message "GNU sed not found. The sed you have is not the GNU version." "0"
        exit 1
      fi
    fi
  else
    # Fatal error, won't continue without any sed at all
    message "sed command not found. Install GNU sed on your system." "0"
    exit 1
  fi
}

# Check for all tools the framework depends on
function check_requirements() {
  message "Checking system requirements" "1"
local toolbox=(
    "chmod"
    "dd"
    "find"
    "gawk"
    "grep"
    "jq"
    "mattrib"
    "mcopy"
    "mdeltree"
    "mdir"
    "mlabel"
    "mmd"
    "mpartition"
    "mformat"
    "readlink"
    "sha256sum"
    "tr"
    "unix2dos"
    "unzip"
    "wget"
    "yamllint"
    "yq"
    "zip"
  )
  for filename in "${toolbox[@]}"; do
    check_utility "$filename"
  done
  detect_gnu_sed
  detect_gnu_grep
  detect_yq
}

# Check and create the BUILDPATH
function make_buildpath() {
  message "Setting up the build environment." "1"
  BUILDPATH=$(eval echo "${BUILDPATH}")

  if [[ ! -d "${BUILDPATH}" ]]; then
    mkdir -p "${BUILDPATH}" > /dev/null 2>&1
  fi
}

# Check and create the DISTPATH
function make_distpath() {
  message "Creating location for distribution files." "1"
  DISTPATH=$(eval echo "${DISTPATH}")
  if [[ -d "${DISTPATH}" ]]; then
    message "DISTPATH '${DISTPATH}' already exists." "2"
    return 0
  fi
  if mkdir -p "${DISTPATH}" > /dev/null 2>&1; then
    message "Created DISTPATH '${DISTPATH}'." "2"
    return 0
  else
    message "Failed to create DISTPATH '${DISTPATH}'." "0"
    return 1
  fi
}

# Download the distfiles for a specific build
function fetch_distfile() {
  # Check if filename and checksum are provided
  if [[ -z "$1" ]] || [[ -z "$2" ]]; then
    message "Usage: fetch_distfile <filename> <sha256sum>" "0"
    return 1
  fi

  local filename="$1"
  local expected_checksum="$2"
  local filepath="$DISTPATH/$filename"

  # Construct the URL based on your file naming convention
  local url="${DIST_BASEURL}/${filename}"  # Replace with your actual URL or file path

  # Check if file already exists
  if [[ -f "$filepath" ]]; then
    # Calculate current checksum
    current_checksum=$("${sha256sum}" "$filepath" | "$gawk" '{print $1}')

    # Compare with the expected checksum
    if [[ "$current_checksum" = "$expected_checksum" ]]; then
      message "${filename} already exists in ${DISTPATH}." "2"
      return 0
    else
      message "Checksum mismatch for ${filename}. Redownloading..." "2"
      rm -f "$filepath"  # Remove the existing file
    fi
  fi

  # Download file
  case $LOGLEVEL in
  "1" | "2")
    "$wget" -q --show-progress -O "$filepath" "$url"
    ;;
  *)
    "$wget" -q -O "$filepath" "$url"
  esac

  # Check if download was successful
  if [ $? -eq 0 ]; then
    message "Downloaded ${filename} to ${DISTPATH}" "2"
  else
    message "Error downloading ${filename} from ${url}" "0"
    rm -f "$filepath"  # Remove incomplete file
    return 1
  fi

  # Verify checksum after download
  downloaded_checksum=$("${sha256sum}" "$filepath" | awk '{print $1}')
  if [[ "$downloaded_checksum" != "$expected_checksum" ]]; then
    message "Checksum verification failed for ${filename}." "0"
    rm -f "$filepath"  # Remove the downloaded file
    return 1
  fi
}

# Extract distfiles
function extract_distfile() {
  message "Extracting distribution file" "1"
  local filename="$1"
  local filepath="$DISTPATH/$filename"

  if [[ -f "$filepath" ]]; then
    "$unzip" -o -q "$filepath" -d "$DISTPATH"
    "$find" "$DISTPATH" -type d -exec "$chmod" 755 {} \;
  else
    message "$filename not available for extraction in $DISTPATH." "0"
    return 1
  fi
}

# Prevent duplication of code since OS-downloading happens in more than 1 place
function download_os() {
    # Download each specific DOS version as needed
    case $OS in
    "MSDOS400")
      fetch_distfile "msdos400.zip" "110f24820365fa5b949ca42da259d87bc9e437ee3d9d86cd15bd1f84a929f8ab"
      if [[ ! -d "$DISTPATH/$OS" ]]; then
        extract_distfile "msdos400.zip"
      fi
      ;;
    "MSDOS401")
      fetch_distfile "msdos401.zip" "99c08dee7a4c421c497466736c17c3243dc4e269d8e4c6c7ca4796af30329857"
      if [[ ! -d "$DISTPATH/$OS" ]]; then
        extract_distfile "msdos401.zip"
      fi
    ;;
    "MSDOS500")
      fetch_distfile "msdos500.zip" "94f5447c9758bed275b8250ba0586927235107ce915b43f49d39c255af674c3a"
      if [[ ! -d "$DISTPATH/$OS" ]]; then
        extract_distfile "msdos500.zip"
      fi
    ;;
    "MSDOS600")
      fetch_distfile "msdos600.zip" "9df5f698d729e24947748feee9e06bfbe54dbeef36eba85f9f5d6d5262d1a03a"
      if [[ ! -d "$DISTPATH/$OS" ]]; then
        extract_distfile "msdos600.zip"
      fi
    ;;
    "MSDOS620")
      fetch_distfile "msdos620.zip" "7e66a0daf3ae5bcf8170a491602f0c3fa93a49927481e4ae323076d72ec34579"
      if [[ ! -d "$DISTPATH/$OS" ]]; then
        extract_distfile "msdos620.zip"
      fi
    ;;
    "MSDOS621")
      fetch_distfile "msdos621.zip" "da7f9bdf7e96d9f445d865f4e6aad4d3cbefa8d8606b554f9e3d2fcfac002c0d"
      if [[ ! -d "$DISTPATH/$OS" ]]; then
        extract_distfile "msdos621.zip"
      fi
    ;;
    "MSDOS622")
      fetch_distfile "msdos622.zip" "af8de768efda551ab8526f71d8835b9d24a8c58c18adb2e9e064f6d0af8c7c4b"
      if [[ ! -d "$DISTPATH/$OS" ]]; then
        extract_distfile "msdos622.zip"
      fi
    ;;
    *)
      message "$OS is not a supported operating system." "0"
      return 1
  esac
}

function cleanup_os() {
  if [[ -d "$DISTPATH/$OS" ]]; then
    clean_path "$DISTPATH/$OS"
  fi
}

# Set up partitions on disk image
function partition_disk_image() {
  message "Partitioning the disk image" "1"
  download_os

  # Do the actual partitioning
  "$mpartition" -I -B "$DISTPATH/$OS/mbr.bin" C: > /dev/null 2>&1
  "$mpartition" -c -a -b 63 c: > /dev/null 2>&1
  "$mformat" -H 63 -B "$DISTPATH/$OS/vbr.bin" c: > /dev/null 2>&1

  # Clean up after ourselves
  cleanup_os
}

function prepare_distfiles() {
  set +u
    if [ "${#DISTFILES[@]}" -gt 0 ]; then
      message "Preparing distribution files" "1"
      for filename in "${!DISTFILES[@]}"; do
        checksum="${DISTFILES[$filename]}"
        fetch_distfile "$filename" "$checksum"
      done

      for filename in "${!DISTFILES[@]}"; do
        extract_distfile "$filename"
      done
    else
      message "No distribution files set. Skipping." "1"
    fi
  set -u
}

function write_os_system() {
  case $OS in
    "MSDOS400" | "MSDOS401" | "MSDOS500" | "MSDOS600" | "MSDOS620" | "MSDOS620" | "MSDOS621" | "MSDOS622")
      message "Writing system files to the disk image." "1"
      download_os
      "$mcopy" "$DISTPATH/$OS/IO.SYS" C:IO.SYS
      "$mcopy" "$DISTPATH/$OS/MSDOS.SYS" C:MSDOS.SYS
      "$mcopy" "$DISTPATH/$OS/COMMAND.COM" C:COMMAND.COM
      "$mattrib" +s +r +h C:IO.SYS
      "$mattrib" +s +r +h C:MSDOS.SYS
      "$mattrib" +a C:COMMAND.COM

      # Handle DriveSpace for MS-DOS 6.22
      if [[ $OS == "MSDOS622" ]]; then
        "$mcopy" "$DISTPATH/$OS/DRVSPACE.BIN" C:DRVSPACE.BIN
        "$mattrib" +s +r +h C:DRVSPACE.BIN
      fi
      "$mlabel" C:"$DISKLABEL"
      cleanup_os
      ;;
    *)
      message "$OS is not a supported operating system." "0"
      return "1"
      ;;
  esac
  case $OS in
    "MSDOS400" | "MSDOS401")
      # SHARE is only needed on drives larger than 32MB in MS-DOS 4.xx
      if [[ $DISKSIZE -gt 31 ]]; then
        message "MS-DOS 4.0x requires SHARE.EXE for drives larger than 32MB. Adding it automatically." "2"
        DRIVERS+=("SHARE")
      fi
    ;;
    *)
      message "Not running on an OS that requires SHARE.EXE. Skipping it." "2"
    ;;
  esac
}

# Update CONFIG.SYS or AUTOEXEC.BAT
update_config() {
    message "Updating MS-DOS system configuration." "1"
    if [[ "$#" -ne 3 ]]; then
        message "Incorrect number of parameters. Usage: update_config <file_type> <text> <position>" "0"
        return 1
    fi

    file_type=$1
    text=$2
    position=$3
    if [[ $CONFIG != *$'\n' ]]; then
      CONFIG+="\r\n"
    fi
    if [[ $AUTOEXEC != *$'\n' ]]; then
      AUTOEXEC+="\r\n"
    fi

    if [[ "$file_type" == "CONFIG" ]]; then
        file_var="CONFIG"
    elif [[ "$file_type" == "AUTOEXEC" ]]; then
        file_var="AUTOEXEC"
    else
        message "Invalid file type. Supported types are CONFIG or AUTOEXEC." "2"
        return 1
    fi

    # Ensure that the added text ends with a newline character
    text_with_newline="$text"$'\n'

    if [[ $position -eq 0 ]]; then
        # Add text at the start of the variable
        eval "$file_var=\"$text_with_newline\${$file_var}\""
    elif [[ $position -eq 1 ]]; then
        # Add text at the end of the variable
        eval "$file_var=\"\${$file_var}$text_with_newline\""
    else
        message "Invalid position. Supported positions are 0 (start) or 1 (end)." "2"
        return 1
    fi
}

# Install and enable SHARE.EXE on MS-DOS 4.xx
function install_share() {
  case $OS in
  "MSDOS400" | "MSDOS401")
    download_os
    if [[ -f "$DISTPATH/$OS/DOS/SHARE.EXE" ]]; then
      "$mcopy" -o "$DISTPATH/$OS/DOS/SHARE.EXE" C:/DOS
      update_config "CONFIG" "INSTALL=C:\DOS\SHARE.EXE /L:500 /F:5100" "0"
    else
      message "Cannot find SHARE.EXE in $DISTPATH/$OS. Aborting!" "0"
      return 1
    fi
    ;;
    *)
      message "SHARE.EXE is only needed on MS-DOS 4.xx. Skipping for $OS." "2"
    ;;
  esac
}

# AO486 FPGA core utilities
function install_ao486() {
  message "Install AO486 FPGA core utilities." "1"
  fetch_distfile "ao486drv.zip" "c337afde4784548becce015854ee70717a1ed287fadeb158580c96d1654ff28e"
  extract_distfile "ao486drv.zip"
  set +e
  "$mdir" C:DRIVERS > /dev/null 2>&1
  if [[ $? -eq 1 ]]; then
    "$mmd" C:DRIVERS
  fi
  set -e
  "$mmd" C:/DRIVERS/AO486
  "$mcopy" "$DISTPATH/drv/imgset.exe" C:/DRIVERS/AO486/IMGSET.EXE
  "$mcopy" "$DISTPATH/drv/misterfs.exe" C:/DRIVERS/AO486/MISTERFS.EXE
  "$mcopy" "$DISTPATH/drv/mpuctl.exe" C:/DRIVERS/AO486/MPUCTL.EXE
  "$mcopy" "$DISTPATH/drv/sbctl.exe" C:/DRIVERS/AO486/SBCTL.EXE
  "$mcopy" "$DISTPATH/drv/sysctl.exe" C:/DRIVERS/AO486/SYSCTL.EXE
  clean_path "$DISTPATH/drv"
}

# SoftMPU installation
function install_softmpu() {
  message "Installing SoftMPU." "1"
  fetch_distfile "softmpu-1.91.zip" "0a7a57f73e3a55a057793252000308683e9b814addf97705902b10dc34560a3c"
  extract_distfile "softmpu-1.91.zip"
  set +e
  "$mdir" C:DRIVERS > /dev/null 2>&1
  if [[ $? -eq 1 ]]; then
    "$mmd" C:DRIVERS
  fi
  set -e
  "$mmd" C:/DRIVERS/SOFTMPU
  "$mcopy" "$DISTPATH/softmpu-1.91/SOFTMPU.EXE" C:/DRIVERS/SOFTMPU/SOFTMPU.EXE
  if [[ "$HAVE_HIMEM" == "0" ]]; then
    message "..using low memory." "2"
    update_config "AUTOEXEC" "C:\DRIVERS\SOFTMPU\SOFTMPU.EXE" "0"
  else
    message "..using high memory." "2"
    update_config "AUTOEXEC" "C:\DRIVERS\SOFTMPU\SOFTMPU.EXE" "0"
  fi
  clean_path "$DISTPATH/softmpu-1.91"
}

# Install full OS if so requested
function install_full_os() {
  case $OS in
  "MSDOS400" | "MSDOS401" | "MSDOS500" | "MSDOS600" | "MSDOS620" | "MSDOS621" | "MSDOS622")
    message "Installing full version of $OS." "1"
    download_os

    # First we remove every DOS file the framework may have already placed
    if "$mdir" C:/DOS > /dev/null 2>&1; then
      "$mdeltree" C:/DOS
    fi

    # Reconstruct the DOS directory
    "$mmd" C:/DOS
    # Copy all of DOS into the DOS directory in the disk image
    "$mcopy" "$DISTPATH/$OS/DOS" C:
    cleanup_os
  ;;
  *)
    message "Cannot install the full version of $OS." "0"
    return 1
  ;;
  esac
}

# CuteMouse installation
function install_cutemouse() {
  message "Installing CuteMouse." "1"
  # Use 21b4 as the default if undefined
  if [[ -z ${CTMOUSE_VERSION+x} ]]; then
    CTMOUSE_VERSION="21b4"
  fi
  case "$CTMOUSE_VERSION" in
    "1.8")
      fetch_distfile "ctmous18.zip" "3471e799f275c43d455923b17d911c4f124b5d77a1b2e8f4b57b2c7639bfb3b0"
      extract_distfile "ctmous18.zip"
      ;;
    "1.91")
      fetch_distfile "cutemouse191.zip" "8cf7379cb7d7f01f3031e589286e920f0e8d9d4f73782f30f9f6af5b21329c66"
      extract_distfile "cutemouse191.zip"
      ;;
    "21b4")
      fetch_distfile "cutemouse21b4.zip" "1d7f04687bafe20d9313db56d15c9df870c055dc63548aa91488e13dd741d50e"
      extract_distfile "cutemouse21b4.zip"
      ;;
    *)
      # Catch-all for if user picked an actually *WRONG* version instead of not defining it at all
      message "Version $CTMOUSE_VERSION is not a valid CuteMouse revision. Not installing anything." "0"
      return 0
      ;;
  esac
  set +e
  "$mdir" C:DRIVERS > /dev/null 2>&1
  if [[ $? -eq 1 ]]; then
    "$mmd" C:DRIVERS
  fi
  set -e
  "$mmd"  c:/DRIVERS/CTMOUSE
  "$mcopy" "$DISTPATH/bin/ctmouse.exe" c:/DRIVERS/CTMOUSE/CTMOUSE.EXE
  if [[ "$HAVE_HIMEM" == "0" ]]; then
    message "..using low memory." "2"
    update_config "AUTOEXEC" "C:\DRIVERS\CTMOUSE\CTMOUSE.EXE" "0"
  else
    message "..using high memory." "2"
    case $OS in
    "MSDOS400" | "MSDOS401")
      update_config "AUTOEXEC" "C:\DRIVERS\CTMOUSE\CTMOUSE.EXE" "0"
      ;;
    "MSDOS500")
      update_config "AUTOEXEC" "LH C:\DRIVERS\CTMOUSE\CTMOUSE.EXE" "0"
      ;;
    *)
      update_config "AUTOEXEC" "LH /S C:\DRIVERS\CTMOUSE\CTMOUSE.EXE" "0"
      ;;
    esac
  fi

  # Not all zips extract the same items into DISTPATH
  if [[ -d "$DISTPATH/bin" ]]; then
    clean_path "$DISTPATH/bin"
  fi
  if [[ -d "$DISTPATH/doc" ]]; then
    clean_path "$DISTPATH/doc"
  fi
  if [[ -d "$DISTPATH/source" ]]; then
    clean_path "$DISTPATH/source"
  fi
}

# Install Q87
function install_q87() {
  message "Installing Q87." "1"
  fetch_distfile "q87.zip" "cb440ea3e4bc9cf03738c6071d8fd1e8490ff56a37a41abc1e0ec47f6de20176"
  extract_distfile "q87.zip"
  set +e
  "$mdir" C:DRIVERS > /dev/null 2>&1
  if [ $? -eq 1 ]; then
    "$mmd" C:DRIVERS
  fi
  set -e
  "$mmd" C:/DRIVERS/Q87
  "$mcopy" "$DISTPATH/Q87/Q87.EXE" C:/DRIVERS/Q87/Q87.EXE
  if [[ "$HAVE_HIMEM" == "0" ]]; then
    message "..using low memory." "2"
    update_config "AUTOEXEC" "C:\DRIVERS\Q87\Q87.EXE" "0"
  else
    message "..using high memory." "2"
    update_config "AUTOEXEC" "LOADHIGH C:\DRIVERS\Q87\Q87.EXE" "0"
  fi
  clean_path "$DISTPATH/Q87"
}

# Install the HIMEM.SYS driver
function install_himem() {
  message "Installing HIMEM." "1"
  download_os

  # This is a weird construct because we cannot directly check for a dir to exist in the DOS image
  set +e
  "$mdir" C:DOS > /dev/null 2>&1
  if [[ $? -eq 1 ]]; then
    "$mmd" C:DOS
  fi
  set -e
  "$mcopy" -o "$DISTPATH/$OS/DOS/HIMEM.SYS" C:/DOS/HIMEM.SYS
  update_config "CONFIG" "DEVICE=C:\DOS\HIMEM.SYS /TESTMEM:OFF" "0"
  cleanup_os
  HAVE_HIMEM=1
}

# Install EMM386
function install_emm386() {
  message "Installing EMM386" "1"
  if [[ "$HAVE_HIMEM" == "0" ]]; then
    install_himem
  fi
  download_os
  set +e
  "$mdir" C:DOS > /dev/null 2>&1
  if [[ $? -eq 1 ]]; then
    "$mmd" C:DOS
  fi
  set -e
  case $OS in
    "MSDOS400" | "MSDOS401")
      "$mcopy" -o "$DISTPATH/$OS/DOS/EMM386.SYS" C:/DOS/EMM386.SYS
      if [[ -z ${EMM386PARAMS+x} ]]; then
        update_config "CONFIG" 'DEVICE=C:\DOS\EMM386.SYS' "1"
      else
        update_config "CONFIG" 'DEVICE=C:\DOS\\\EMM386.SYS '"$EMM386PARAMS"'' "1"
      fi
      ;;
    "MSDOS500" | "MSDOS600" | "MSDOS620" | "MSDOS621" | "MSDOS622")
      "$mcopy" -o "$DISTPATH/$OS/DOS/EMM386.EXE" C:/DOS/EMM386.EXE
      if [[ -z ${EMM386PARAMS+x} ]]; then
        update_config "CONFIG" 'DEVICEHIGH=C:\DOS\EMM386.EXE i=e000-efff' "1"
      else
        update_config "CONFIG" 'DEVICEHIGH=C:\DOS\\\EMM386.EXE '"$EMM386PARAMS"'' "1"
      fi
      ;;
    *)
      message "EMM386 not supported on ${OS}" "0"
      cleanup_os
      return 1
  esac
  cleanup_os
}

# CD-ROM support
function install_cdrom() {
  case "$OS" in
  "MSDOS600" | "MSDOS620" | "MSDOS621" | "MSDOS622")
    download_os
        set +e
    "$mdir" C:DOS > /dev/null 2>&1
    if [[ $? -eq 1 ]]; then
      "$mmd" C:DOS
    fi
    set -e
    fetch_distfile "apicd214.zip" "76789ca220d4f53b23be265362e81063fb4b40144b89c1e4d5d7d3588188f88b"
    extract_distfile "apicd214.zip"
    set +e
    "$mdir" C:DRIVERS > /dev/null 2>&1
    if [[ $? -eq 1 ]]; then
      "$mmd" C:DRIVERS
    fi
    set -e
    "$mmd" C:/DRIVERS/CDROM
    "$mcopy" "$DISTPATH/apicd214/VIDE-CDD.SYS" C:/DRIVERS/CDROM/VIDE-CDD.SYS
    if [[ "$HAVE_HIMEM" == 0 ]]; then
      update_config "CONFIG" "DEVICE=C:\DRIVERS\CDROM\VIDE-CDD.SYS /D:MSCD001" "1"
    else
      update_config "CONFIG" "DEVICEHIGH=C:\DRIVERS\CDROM\VIDE-CDD.SYS /D:MSCD001" "1"
    fi
    "$mcopy" -o "$DISTPATH/$OS/DOS/MSCDEX.EXE" C:/DOS/MSCDEX.EXE
    if [[ "$HAVE_HIMEM" == "0" ]]; then
      update_config "AUTOEXEC" "C:\DOS\MSCDEX.EXE /D:MSCD001" "0"
    else
      update_config "AUTOEXEC" "LH C:\DOS\MSCDEX.EXE /D:MSCD001" "0"
    fi
    cleanup_os
    ;;
  *)
    message "CD-ROM support not available in $OS. Currently only for MS-DOS 6.00 and up." "1"
    return 1
    ;;
  esac
}

# Install the CHOICE utility to the disk image
function install_choice() {
  message "Installing CHOICE utlity." "1"
  case "$OS" in
    "MSDOS400" | "MSDOS401" | "MSDOS500")
      message "MS-DOS < 6.0 does not have CHOICE utility. Installing FreeDOS version." "1"
      fetch_distfile "choice_freedos.zip" "f2648e717f52dbc580840f77a1aec4c2303a66bd1bcb9b6b871e17514e733469"
      extract_distfile "choice_freedos.zip"
      set +e
      "$mdir" C:DOS > /dev/null 2>&1
      if [[ $? -eq 1 ]]; then
        "$mmd" C:DOS
      fi
      set -e
      # Note that CHOICE is an EXE in FreeDOS and a COM in MS-DOS!
      "$mcopy" "$DISTPATH/choice/BIN/_CHOICE.EXE" C:/DOS/CHOICE.EXE
      clean_path "$DISTPATH/choice"
      return 0
      ;;
    "MSDOS600" | "MSDOS620" | "MSDOS621" | "MSDOS622" )
      download_os
      set +e
      "$mdir" C:DOS > /dev/null 2>&1
      if [[ $? -eq 1 ]]; then
        "$mmd" C:DOS
      fi
      set -e
      "$mcopy" "$DISTPATH/$OS/DOS/CHOICE.COM" C:/DOS/CHOICE.COM
      cleanup_os
      return 0
      ;;
    *)
      message "CHOICE is not supported on $OS." "0"
      return 1
      ;;
  esac
}

# Install device drivers
function install_drivers() {
  for driver in "${DRIVERS[@]}"
  do
    case $driver in
      "SHARE")
        install_share
        ;;
      "AO486")
        install_ao486
        ;;
      "EMM386")
        install_emm386
        ;;
      "HIMEM")
        install_himem
        ;;
      "CUTEMOUSE")
        install_cutemouse
        ;;
      "CHOICE")
        install_choice
        ;;
      "Q87")
        install_q87
        ;;
      "CDROM")
        install_cdrom
        ;;
      "SOFTMPU")
        install_softmpu
        ;;
      *)
        message "$driver is not a known keyword for use in DRIVERS, skipping over it." "2"
        ;;
    esac
  done
}

# Write AUTOEXEC.BAT to the disk image
function write_autoexec() {
  echo -e "$AUTOEXEC" > "$DISTPATH"/"AUTOEXEC.BAT"
  "$sed" -i '/^[[:space:]]*$/d' "$DISTPATH/AUTOEXEC.BAT"
  "$unix2dos" "$DISTPATH/AUTOEXEC.BAT" > /dev/null 2>&1
  "$mcopy" "$DISTPATH/AUTOEXEC.BAT" C:AUTOEXEC.BAT
}

# Write CONFIG.SYS to the disk image
function write_config() {
  echo -e "$CONFIG" > "$DISTPATH"/"CONFIG.SYS"
  "$sed" -i '/^[[:space:]]*$/d' "$DISTPATH/CONFIG.SYS"
  "$unix2dos" -f "$DISTPATH/CONFIG.SYS" > /dev/null 2>&1
  "$mcopy" "$DISTPATH/CONFIG.SYS" C:CONFIG.SYS
}

# Copy the game to the disk image
function install_game() {
  for file in "${GAMEFILES[@]}"
  do
    "$mcopy" -s "$DISTPATH"/$file c:
  done
}

# Create disk image file
function create_disk_image() {
  message "Creating the disk image." "1"
  # Check if the correct number of parameters is provided
  if [[ "$#" -ne 2 ]]; then
    message "Usage: create_disk_image <filename> <size_in_MB>" "0"
    return 1
  fi

  local filename="$1"
  local filesize="$2"

  # Validate filename
  if [[ -z "$filename" ]]; then
    message "Error: Filename cannot be empty." "0"
    return 1
  fi

  # Validate size_in_MB
  if ! [[ "$filesize" =~ ^[0-9]+$ ]]; then
    message "Error: Size must be a positive integer." "0"
    return 1
  fi

  # Check if the size is greater than 0
  if [[ "$filesize" -le 0 ]]; then
    message "Error: Size must be greater than 0." "0"
    return 1
  fi

  # Check if the size is less than or equal to 2047 MB
  case $OS in
    "MSDOS400" | "MSDOS401")
      if [[ "$filesize" -gt 512 ]]; then
        message "Error: Size must not exceed 512 MB. for MS-DOS 4.0x." "0"
        return 1
      fi
      ;;
    *)
      if [[ "$filesize" -gt 2047 ]]; then
        message "Error: Size must not exceed 2047 MB." "0"
        return 1
      fi
    ;;
  esac

  # Create an empty disk image
  "$dd" if=/dev/zero of="${BUILDPATH}/${filename}" bs=1M count=$filesize 2> /dev/null

  # Prepare the mtools configuration
  if [[ -f "~/.mtoolsrc" ]]; then
    mv ~/.mtoolsrc ~/.mtoolsrc_orig
  fi
  
  echo "drive c: file=\"${BUILDPATH}/${filename}\" partition=1" > ~/.mtoolsrc
  echo "MTOOLS_NO_VFAT=1" >> ~/.mtoolsrc
}

# Create a ZIP-file of the disk image
function compress_image() {
  if [[ -f "${BUILDPATH}/${DISKNAME}" ]]; then
    "$zip" -j "${BUILDPATH}/${DISKNAME%.*}.zip" "${BUILDPATH}/${DISKNAME}" > /dev/null 2>&1
    message "Created ${BUILDPATH}/${DISKNAME%.*}.zip" "0"
  else
    message "Error: Failed to create ZIP archive of ${DISKNAME}." "0"
    return 1
  fi
}

# Remove temporary files from the build environment
function cleanup_build() {
  message "Cleaning up the build environment." "1"
  if [[ -f "$DISTPATH/AUTOEXEC.BAT" ]]; then
    message "Removing AUTOEXEC.BAT" "2"
    rm "$DISTPATH/AUTOEXEC.BAT"
  fi
  if [[ -f "$DISTPATH/CONFIG.SYS" ]]; then
    message "Removing CONFIG.SYS" "2"
    rm "$DISTPATH/CONFIG.SYS"
  fi
  for filename in "${GAMEFILES[@]}"; do
    if [[ -d "$DISTPATH/$filename" ]]; then
      clean_path "$DISTPATH/$filename"
    fi
    if [[ -f "$DISTPATH/$filename" ]]; then
      rm "$DISTPATH/$filename"
    fi
  done
}
