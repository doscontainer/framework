#!/usr/bin/env bash
# Copyright 2023 Bas v.d. Wiel
#
# Redistribution and use in source and binary forms, with 
# or without modification, are permitted provided that 
# the following conditions are met:
#
# 1. Redistributions of source code must retain the above 
#    copyright notice, this list of conditions and the following 
#    disclaimer.
#
# 2. Redistributions in binary form must reproduce the above 
#    copyright notice, this list of conditions and the following 
#    disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its 
#    contributors may be used to endorse or promote products 
#    derived from this software without specific prior written 
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
# CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, 
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
# OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

set -eu

# Default values for variables that may be overwritten from the config
DOSK8SGUARD="1"
BUILDPATH="${BUILDPATH:-~/doscontainer}"
expanded_path=$(eval echo "${BUILDPATH}")
DISTPATH="${BUILDPATH}/distfiles"
DISKLABEL="${DISKLABEL:-DOSK8S}"
GAMEPATH="${GAMEPATH:-GAME}"
HAVE_HIMEM=0
OS="${OS:-MSDOS622}"
OSVARIANT="${OSVARIANT:-MINIMAL}"
DIST_BASEURL="${DIST_BASEURL:-ftp://ftp.area536.com/doscontainer/distfiles/}"

if [[ "$#" -lt 1 ]]; then
    echo "Error: Configuration file not provided. Please pass a configuration file as a parameter."
    exit 1
fi

if [[ ! -r "$1" ]]; then
  echo "Error: cannot read configuration file."
  exit 1
fi

# Pull in the DOSContainer config file if present
# First look in the user's homedir for a dotfile
if [[ -r ~/.doscontainerrc ]]; then
  source ~/.doscontainerrc
fi

# ..then in the current dir for one that could be commited to a repo.
# The closer a config file is to the build script, the more priority it gets.
if [[ -r doscontainer.conf ]]; then
  source doscontainer.conf
fi

# Set a sensible default LOGLEVEL if one isn't present at this point
: "${LOGLEVEL:=0}"

# Load the framework function library
source framework.sh

# Perform the image build
check_requirements
parse_yaml "$1"
print_intro
make_buildpath
make_distpath
prepare_distfiles
create_disk_image "$DISKNAME" $DISKSIZE
partition_disk_image
write_os_system
if [[ $OSVARIANT == "FULL" ]]; then
  install_full_os
fi
install_drivers
install_game
write_config
write_autoexec
compress_image
cleanup_build
