# Creating game configuration files

Game configuration files take the form of YAML documents in which you declare what you
want DOSContainer to build for you. I chose this format because YAML is the lingua franca
for all sorts of similarly declarative systems. Some familiarity with it is therefore
assumed. Your friendly neighborhood search engine will gladly provide you with thousands
of tutorials, blogs and videos on the format.

So I said 'declarative', right? By that I mean that you only describe *what* you want the
script to build, not *how* to build it. No exceptions! The reason for this is that I do not
want the game configuration format to introduce any kind of executable code that will run
on the build machine (ie. your laptop) unguarded. Sure you're not running as 'root', right?
Well, any script that runs as your own user can still destroy all the files you own.. probably
not something you look forward to.

## YAML specification

This is where the dragons still are. Not everything you see here is set in stone yet, so
I caution you agains investing too much into creating configs of your own just yet or be
prepared to face some major flux over the next few releases!

### Metadata

The whole 'metadata' section is unused for now. It's in there so configs can be indexed
more easily later on as I intend to build a web page for them. For now: ignore it.

### Disk properties

| Variable | Description |
|----------|-------------|
| diskname | Name of the disk image on your host's storage. |
| disksize | Size of the disk image in megabytes. |
| disklabel | MS-DOS disk label. Max. 8 characters, all caps. |

### Distribution files

The ```distfiles``` list any number of downloadable resources (zip-files only for now)
that the framework pulls in and extracts. The checksum is the SHA256 sum of the file to
prevent errors and unnecessary downloading.

All distribution files will be saved in ```~/doscontainer/distfiles```. Anything not
present there will be pulled in from my anonymous FTP server. On that note, you're welcome
to browse ```ftp.area536.com```. The doscontainer bits are in ```doscontainer/distfiles``` 
for all to see.

### Game files

The ```gamefiles``` list refers to any file and directory inside the zip-files that we want
to have copied into the disk image. Usually just the top-level directory where the game was
installed.

### Operating system

The ``os`` variable can be anything from the following list:

- MSDOS400
- MSDOS401
- MSDOS500
- MSDOS600
- MSDOS620
- MSDOS621
- MSDOS622

This determines what system files will be put on the disk image for the emulator to boot up. It
very explicitly does *NOT* install anything else. If you want to have the kitchen sink, you can
set `osvariant` to ```FULL``` and get a completely populated ```C:\DOS``` directory.

For most games a full DOS installation is less than useless, so I'm skipping it by default.

### Drivers and utilities

DOSContainer facilitates a growing number of drivers and utilities that are needed to run all sorts
of old games. The supported ones at this time of writing are:

- HIMEM
- EMM386
- Q87
- CDROM
- AO486
- CUTEMOUSE
- CHOICE
- SOFTMPU
- SHARE

**HIMEM** and **EMM386** are components of MS-DOS for memory management that will get installed and configured
if you put them in ```drivers```. No need to tweak ```CONFIG.SYS``` or ```AUTOEXEC.BAT``` on your own for
them to work.

The framework sets sensible defaults for EMM386 on its own but you can also choose to tweak them yourself. In
order to do so, you can set ```emm386params``` to any string value you want. Defining this setting flags to the
framework that you know what you're doing, and none of the defaults will be passed to EMM386. Your parameters
will be appended *verbatim* to the line that loads EMM386 in ```CONFIG.SYS``` or ```AUTOEXEC.BAT```.

**Q87** is an FPU-emulator in its shareware incarnation. Not extremely useful yet but I added it while
trying to get Falcon 3.0 running and just left it in the framework.

**CDROM** is supported for MS-DOS 6.00 and higher and allows you to use CD-ROM images without doing the work
yourself. It takes care of an IDE CD-driver and the linkup between that and ```MSCDEX.EXE```.

**AO486** refers to the toolbox that MiSTerFPA's AO486 core has available. Adding this installs the tools to
your disk image. Very useful if you want to control the core from within DOS.

**CUTEMOUSE** installs the CuteMouse mouse driver. By default it installs the very latest beta version of the
driver, which is 21b4 at the time of writing. You can use '1.8', '1.91' or '21b4' right now or simply omit
the ```cutemouseversion``` field entirely from your YAML. If you enter a version that DOSContainer does not
know about, it won't install *anything at all* and throw you a message about it.

**CHOICE** is required for every game that has a boot menu from a .BAT file where user input is processed through
CHOICE. Not every version of MS-DOS included it. Setting this in ```drivers``` pulls in the original Microsoft
version of CHOICE, or places the FreeDOS version into ```C:\DOS``` for ancient versions of the OS.

**SOFTPMU** for Roland MPU-401 MIDI emulation. You'll have to wire this into your environment manually but it'll 
get installed for you.

**SHARE** This applies only to MS-DOS 4.0x and is handled automatically for the use cases where it applies: partition
sizes larger than 32MB. The framework will detect these for you. You can opt to install SHARE.EXE on smaller
partitions as well by including this driver in your ```drivers``` list if you really want to.

## Setting up your development environment

Use whatever DOS environment you feel comfortable with, and are able to get files back out of. On my laptop I
use PCem and 86Box and the GNU Mtools (which are present anyway for DOSContainer). My own general flow looks
something like this:

- Estimate the size of the disk I need.
- Copy a suitable blank MS-DOS YAML to the name of the game I'm porting.
- Run DOSContainer to create a blank MS-DOS environment.
- Install the game in there, the old-school way.
- Copy the installed directory back out, usually using ```mcopy```.
- Create a zipfile of the extracted directory.
- Run ```sha256sum``` on the newly created zipfile.
- Copy(!) the zipfile into ```~/doscontainer/distfiles```
- Set up the ```distfile``` and ```gamefiles``` arrays in the YAML.
- Run DOSContainer on the new YAML.
- Test the newly generated image
- Create a ```DOSK8S``` directory inside the game's installed dir.
- Write a ```RUN.BAT``` file in there from within MS-DOS that will constitute the boot menu.
- Setup the ```AUTOEXEC``` variable in YAML to call your new ```RUN.BAT```.

You can write your YAML in just about any editor imaginable. Do note at all times that whatever text files
you write in Linux will need to be pulled through ```unix2dos``` to fix line endings! The ```AUTOEXEC``` and
```CONFIG``` variables in the YAML configs get this treatment automatically from the framework, but anything
else you add to your zipfile will not!

Most boot menus copy around different sets of game-specific configuration files, according to a simple choice
made by the end user for things like sound hardware. It's tedious to create, but it makes the experience of
using the games just that much simpler for the end user.

The base expectation is that the system boots into the game as automagically as possible, only bothering the
user with choices that really cannot be autodetected from the environment. I also add an option to any boot
menu to quit to DOS before starting the game, but that's more of a courtesy. Quitting the game itself usually
has the same effect but isn't always available very quickly.

## Getting your config included

During development your zipfile can live in your local ```distfiles``` directory and your builds will work
just fine. As long as the checksum matches that of the file, DOSContainer won't attempt (and fail!) to download
the file from my server.

You have effectively two choices here:

1. Pull all distfiles from my FTP box and start hosting your own. Adjust build.sh for your local environment so
the base URL points to your own repository. As long as an anonymous ```wget``` can get to it from wehere you
sit, things will work just dandy. Disadvantage: you'll need to keep things in sync with upstream yourself and
I'm not offering ```rsync``` (yet).

2. Report an issue to the ```games``` project in GitLab and I'll get back to you for now. Any decent quality game
config is more than welcome and I'll gladly add it to the repository.

In regards to option 2 I have a small number of requirements to ensure quality and integrity of the collection:

- No software that was released less than 25 years ago. That's somewhat arbitrary, I know, but I really am not
here to facilitate piracy. This is a retro hobby and I want to keep things *retro*.
- Only base yourself off files that can be found in public sources that can be found elsewhere, most preferably the
repository at Archive.org and please provide a link to your source.
- Don't include cracks or cracked versions, except for copy protection schemes that are based on physical properties
of real machines that can't be duplicated in emulation like weird floppy tracks, dongles and the like.
- Try to find the most authentic versions of games available. If you want to include mods or fan patches, that's fine
but please create separate YAML's and zipfiles for those.