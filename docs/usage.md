# DOSContainer usage

If you're an end user, this is the right place for you to find out how to
use DOSContainer for your retro computing hobby. This document starts out
with a quick instruction on how you get set-up and what to type so you can
start playing the games of yesteryear. The second section of the document
goes into more detail on what the framework does, so you can start building
your own game configurations.

## System requirements

I wrote the script on two computers. One of them runs Gentoo Linux, the other
runs FreeBSD. Things work on both of them, so I'm quite confident that the
script itself is robust enough to run anywhere as long as you have a fairly
recent version of the Bash shell and the listed external utilities available:

- chmod
- dd
- find
- gawk
- sed (GNU version, install gsed on BSD's like MacOS)
- jq
- mattrib
- mcopy
- mdeltree
- mdir
- mlabel
- mmd
- mpartition
- mformat
- readlink
- sha256sum
- tr
- unix2dos
- unzip
- wget
- yamllint
- yq
- zip

Depending on your operating system, you'll find them in packages that have
names very much like the names in the list. The whole bunch of commands that
start with an 'm' comes from GNU Mtools. Others may or may not come by default
with the installation of your OS. If you have any issues, let me know and I'll
look into helping you out and writing a bit of docs for your specific case.

## Usage

Once that's all settled (the script will tell you and abort mission if things
are not ok), you can start using DOSContainer. For that you need the framework
and at least a single game YAML. Those can be had from the 'games' repository
in the same place where you got the framework. It's where I maintain and update
a growing list of working configs. But let's just start with a single example:

```
---
metadata:
  title: Leisure Suit Larry in the Land of the Lounge Lizards
  publisher: Sierra OnLine
  year: 1987
  comment: Original EGA version
diskname: lsl1.vhd
disksize: 1
distfiles:
  - name: lsl1.zip
    checksum: 0316fb862c67fdf9318a5c8513d99e5af185ce10306d20c27f5c6da099b5b176
gamefiles:
  - LSL1
os: MSDOS400
AUTOEXEC: |
  PATH=C:\DOS
  COMSPEC=C:\COMMAND.COM
  cd C:\LSL1
  SIERRA
CONFIG: |
```

Save this to a .YAML file, and feed it into DOSContainer like so:

```
$./build.sh lsl1.yaml
```

The end result will be a new file in your home folder named ```doscontainer```. Inside
you'll find a qfg1.vhd file that you can pop into your emulator and it'll autoboot the
game for you. That's it. There really is nothing more to it.

## Operating system specific guidance

### macOS 12+

MacOS is a UNIX-like operating system that *should* be a great home for a script like
this one that has its roots in the UNIX world. Unfortunately, things are not that
simple. You're going to need a bunch of external tools, which can be quite easily
acquired and kept up-to-date using Homebrew (see https://brew.sh). The installation
and use of Homebrew is out of scope here, but you may get tripped up by a technicality
due to how macOS and Apple handle the licensing of Bash.

When you ```brew install bash```, as one would normally do, it gives you Bash 3.x by
default which is too old for this script. You really need at least 4.x and the development
is done on 5.x. Best to get a more recent version. As I'm not a Mac user myself, have
a look [here](https://medium.com/@sisiliang/watch-out-these-details-when-upgrading-to-bash-5-on-mac-m1-m2-56bcabcfc549)
to fix this on your system.

Once you get Bash 5 plus the other dependencies up and running, nothing should stop
you from using DOSContainer like on any other UNIX-like system.