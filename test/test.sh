#!/usr/bin/env bash

# Copyright 2023 Bas v.d. Wiel
#
# Redistribution and use in source and binary forms, with 
# or without modification, are permitted provided that 
# the following conditions are met:
#
# 1. Redistributions of source code must retain the above 
#    copyright notice, this list of conditions and the following 
#    disclaimer.
#
# 2. Redistributions in binary form must reproduce the above 
#    copyright notice, this list of conditions and the following 
#    disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its 
#    contributors may be used to endorse or promote products 
#    derived from this software without specific prior written 
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
# CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, 
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
# OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Set DOSK8SGUARD so that the framework recognizes our script
# as a DOSK8S component.
DOSK8SGUARD="1"

# Load the DOSK8S Framework
source ../framework.sh
    set +eu

# Message function
# We have 3 log levels that have different outputs depending
# on the LOGLEVEL variable.

# Message at loglevel 2 is printed when LOGLEVEL is indeed 2.
function testMessageLoglevel2_valid() {
    check_requirements > /dev/null
    LOGLEVEL="2"
    local result=$(message "Test" "2")
    assertEquals "Test" "$result"
}

# Message at loglevel 2 is *NOT* printed at LOGLEVEL = 1
function testMessageLoglevel2_invalid_1() {
    check_requirements > /dev/null
    LOGLEVEL="1"
    local result=$(message "Test" "2")
    assertNull "$result"
}

# Message at loglevel 2 is *NOT* printed at LOGLEVEL = 0
function testMessageLoglevel2_invalid_2() {
    check_requirements > /dev/null
    LOGLEVEL="0"
    local result=$(message "Test" "2")
    assertNull "$result"
}

# Message at loglevel 1 is printed when LOGLEVEL is 2
function testMesageLoglevel1_valid_2() {
    check_requirements > /dev/null
    LOGLEVEL="2"
    local result=$(message "Test" "1")
    assertEquals "Test" "$result"
}

# Message at loglevel 1 is printed when LOGLEVEL is 1
function testMessageLoglevel1_valid_1() {
    check_requirements > /dev/null
    LOGLEVEL="1"
    local result=$(message "Test" "1")
    assertEquals "Test" "$result"
}

# Message at loglevel 1 is *NOT* printed when LOGLEVEL is 0
function testMessageLoglevel1_invalid_0() {
    check_requirements > /dev/null
    LOGLEVEL="0"
    local result=$(message "Test" "1")
    assertNull "$result"
}

# Message at loglevel 0 is printed when LOGLEVEL is 2
function testMesageLoglevel0_valid_2() {
    check_requirements > /dev/null
    LOGLEVEL="2"
    local result=$(message "Test" "0")
    assertEquals "Test" "$result"
}

# Message at loglevel 0 is printed when LOGLEVEL is 1
function testMessageLoglevel0_valid1() {
    check_requirements > /dev/null
    LOGLEVEL="1"
    local result=$(message "Test" "0")
    assertEquals "Test" "$result"
}

# Message at loglevel 0 is printed when LOGLEVEL is 0
function testMessageLoglevel0_valid0() {
    check_requirements > /dev/null
    LOGLEVEL="0"
    local result=$(message "Test" "0")
    assertEquals "Test" "$result"
}

# Test nonsense value for LOGLEVEL
function testMessageLoglevelInvalid() {
    check_requirements > /dev/null
    LOGLEVEL="CLEARLYINVALID"
    local result=$(message "Test" "0")
    assertEquals "Error: Log level and verbosity must be integers between 0 and 2." "$result"
}

# Test out of range LOGLEVEL
function testMessageLoglevelOutOfRange() {
    check_requirements > /dev/null
    LOGLEVEL="6"
    local result=$(message "Test" "0")
    assertEquals "Error: Log level and verbosity must be integers between 0 and 2." "$result"
}

# Message function gets no loglevel parameter
function testMessageNoLoglevel() {
    check_requirements > /dev/null
    LOGLEVEL="0"
    local result=$(message "Test")
    assertEquals "Error: Log level and verbosity must be integers between 0 and 2." "$result"
}

# Message function got not message given, just a loglevel
function testMessageNoMessage() {
    check_requirements > /dev/null
    LOGLEVEL="0"
    local result=$(message "0")
    assertEquals "Error: Log level and verbosity must be integers between 0 and 2." "$result"
}

# Tests for path_is_inside

# Test for a valid case
test_path_is_inside_PathIsInside() {
  check_requirements > /dev/null
  assertTrue "path_is_inside /usr /usr/local"
}

test_path_is_inside_PathIsNotInside() {
    check_requirements > /dev/null
    assertFalse "path_is_inside /usr/local /usr"
}

test_path_is_inside_NotEnoughParameters() {
  check_requirements > /dev/null
  assertFalse "path_is_inside /tmp"
}

test_path_is_inside_NonsensicalParameters() {
    check_requirements > /dev/null
    assertFalse "path_is_inside thisistotally 2455q345"
}

## Tests for validate_yaml
test_validate_yaml_Valid() {
    check_requirements
    local tmpfile=$(mktemp)
    echo "key: value" > "$tmpfile"
    validate_yaml "$tmpfile"
    assertEquals "Expected return code 0 for valid YAML." 0 $?
    rm "$tmpfile"
}

test_validate_yaml_Invalid() {
    check_requirements
    local tmpfile=$(mktemp)
    echo "INVALID YAML: : :: STRUCTURE!" > "$tmpfile"
    validate_yaml "$tmpfile"
    assertEquals "Expected return code 1 for invalid YAML." 1 $?
    rm "$tmpfile"
}

test_validate_yaml_UnreadableFile() {
    check_requirements
    local tmpfile="/tmp/sadfqwrwadfasdfqea.yaml"
    validate_yaml "$tmpfile"
    assertEquals "Expected return code 1 for file that cannot be read." 1 $?
}

# Mock rm so we don't touch the filesystem

test_clean_path_ValidParams() {
    rm() {
      echo "Mock deletion: rm $*"
      return 0
    }

    local DISTPATH="/tmp"
    check_requirements > /dev/null
    assertTrue "clean_path /tmp/flos"
}

test_clean_path_InvalidParams() {
    rm() {
    echo "Mock deletion: rm $*"
    return 0
    }

    local DISTPATH="/usr"
    check_requirements > /dev/null
    assertFalse "clean_path /tmp/flos"
}

## Tests for parse_yaml
test_parse_yaml_NoParams() {
    check_requirements > /dev/null
    assertFalse "parse_yaml"
}

test_parse_yaml_NonsensicalParam() {
    check_requirements > /dev/null
    assertFalse "parse_yaml q34thwpadfasdfarg3qrgasdf"
}

# Run the tests
. ./shunit2